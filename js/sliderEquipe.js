$(function(){


	var amt;
	var currentIndex = 0;
	var delay = 3000;

	function initSlider(){

		amt = $('.sobre-autor').length;

		var sizeContainer = 100 * amt;

		var sizeBoxSingle = 100 / amt;

		$('.sobre-autor').css('width',sizeBoxSingle+'%');
		$('.scroll-wraper').css('width',sizeContainer+'%');

		for( var i = 0; i < amt; i++){
			if (i == 0) {
				$('.slider-bullets').append('<span style="background-color:rgb(90,90,90);"></span>');
			} else{
				$('.slider-bullets').append('<span></span>');
			}
		}
	}

	function autoPlay(){
		setInterval(function(){
			currentIndex++;
			if (currentIndex == amt) {
				currentIndex = 0;
			}
			goToSlider(currentIndex);
		},delay);
	}

	function goToSlider(currentIndex){
		
		var offSetX = $('.sobre-autor').eq(currentIndex).offset().left - $('.scroll-wraper').offset().left;
		$('.slider-bullets span').css('background-color','rgb(200,200,200)');
		$('.slider-bullets span').eq(currentIndex).css('background-color','rgb(90,90,90)');
		$('.scrollEquipe').stop().animate({'scrollLeft':offSetX});

	}

	$(window).resize(function(){

		$('.scrollEquipe').stop().animate({'scrollLeft':0});

	});	

	initSlider();
	autoPlay();

});